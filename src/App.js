import React, { useState } from "react";
import "./App.scss";
import Header from "./component/Header";
import ModalAddNew from "./component/ModalAddNew";
import TableUser from "./component/TableUser";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
function App() {
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
  };

  return (
    <div className="app-container">
      <Header></Header>

      <Container>
        <Button variant="success" onClick={() => setShow(true)}>
          Add User
        </Button>{" "}
        <TableUser></TableUser>
      </Container>
      <ModalAddNew show={show} handleClose={handleClose}></ModalAddNew>
    </div>
  );
}

export default App;
